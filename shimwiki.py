from __future__ import print_function, with_statement

import http.server
import os
import socketserver
import subprocess
import sys
import re
import time

################################################## shimwiki server

class ShimwikiServer(object):
    def __init__(self, directory=None, port=None):
        if directory is None:
            raise ArgumentException('no format specified')
        if port is None:
            raise ArgumentException('no port specified')
        self.directory = directory
        self.port = port

    def run(self):
        with socketserver.TCPServer(('', self.port), http.server.SimpleHTTPRequestHandler) as httpd:
            print(time.asctime(), 'Server Starts - {}:{}'.format('localhost', self.port))
            try:
                httpd.serve_forever()
            except KeyboardInterrupt:
                pass
            httpd.server_close()
            print(time.asctime(), 'Server Stops - {}:{}'.format('localhost', self.port))

################################################## wiki converter

class WikiConverter(object):
    def __init__(self, format=None, input_directory=None, output_directory=None):
        self.error = None
        if format is None:
            raise ArgumentException('no format specified')
        if input_directory is None:
            raise ArgumentException('no input directory specified')
        if output_directory is None:
            raise ArgumentException('no output directory specified')
        self.format = format
        self.input_directory = input_directory
        self.output_directory = output_directory
    def convert(self):
        # Currently assuming a single level of wiki entries...
        exceptions = ('.git',)
        input_files = [file for file in os.listdir(self.input_directory) if file.endswith('.wiki')]
        non_input_files = [file for file in os.listdir(self.input_directory) if not file.endswith('.wiki') and file not in exceptions]
        if len(non_input_files) > 0:
            self.error = 'Warning: {} files in the input directory do not appear to be wiki entries and will not be converted: {}'.format(len(non_input_files), ', '.join(non_input_files))
            print(self.error)
            return
        output_files = []
        for file in input_files:
            current_file_path = os.path.join(self.input_directory, file)
            new_file_name = file[:-len('.wiki')] + '.html'
            new_file_path = os.path.join(self.output_directory, new_file_name)
            command = ('pandoc', current_file_path, '-f', 'vimwiki', '-t', 'html', '-s', '-o', new_file_path)
            output_files.append(new_file_path)
            subprocess.run(command)
            print('Converting:', file)
        manifest_fields = []
        for output_file in output_files:
            instrument(output_file, 'html', 'manifest="manifest.appcache"')
            short_name = os.path.basename(output_file)
            manifest_fields.append(short_name)
        manifest_filename = os.path.join(self.output_directory, 'manifest.appcache')
        build_manifest(manifest_fields, manifest_filename)


def instrument(file, tag, attribute_to_add):
    with open(file, 'r') as f:
        data = f.read()
    html_tag = re.compile(r'<{}[^>]*>'.format(tag), flags=re.IGNORECASE)
    match = html_tag.search(data)
    new_data = data[:match.end() - 1] + ' ' + attribute_to_add + '>' + data[match.end():]
    with open(file, 'w') as f:
        f.write(new_data)


def build_manifest(files, manifest_filename):
    with open(manifest_filename, 'w') as f:
        f.write('CACHE MANIFEST\n')
        for file in files:
            f.write(file + '\n')


################################################## common code

class ArgumentException(Exception):
    pass

################################################## main stuff

def create_vimwiki_converter():
    converter = WikiConverter(format='vimwiki', input_directory='/Users/sean/vimwiki', output_directory='/Users/sean/vimwiki_html2')
    converter.convert()

def run_shimwiki_server():
    server = ShimwikiServer(directory='/Users/sean/vimwiki_html2', port=8080)
    server.run()

def main(args):
    create_vimwiki_converter()
    run_shimwiki_server()
    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
