# Shimwiki Exporter

Tool to export [Vimwiki](https://github.com/vimwiki/vimwiki)-style wikis.

Input to the exporter is the path to the Vimwiki you wish to convert. It will be converted using [Pandoc](https://pandoc.org/) to HTML, and the HTML will be modified to automatically cache itself for offline access when accessed by a device with support for HTML5 application cache.

## Example Usage

```
# ls ~/vimwiki
index.html
# shimwiki ~/vimwiki 80
shimwiki running
```

Then add to `/etc/hosts` file:

```
127.0.0.1 example.com
```

Then navigate to [http://example.com/index.html](http://example.com/index.html). It will load your wiki. Then, you may close shimwiki. Then, access the wiki on this device from the same URL, and, the cached wiki will be loaded.
